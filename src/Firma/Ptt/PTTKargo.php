<?php

namespace KargoApi\Firma\Ptt;

use KargoApi\Firma\KargoFirmaServis;
use SoapClient;

class PTTKargo implements KargoFirmaServis
{
    private $username;
    private $password;
    private $lang = 'TR';
    private $mod = 'test';
    private $apiAuth;

    private $backendUrl = array(
        "test" => array(
            'PttVeriYukle2' => 'https://pttws.ptt.gov.tr/PttVeriYuklemeTest/services/Sorgu?wsdl',
            'GonderiHareketV2' => 'https://pttws.ptt.gov.tr/GonderiHareketV2Test/services/Sorgu?wsdl',
            'GonderiTakipV2' => 'https://pttws.ptt.gov.tr/GonderiTakipV2Test/services/Sorgu?wsdl',
            'PttBarkodVeriSil' => 'https://pttws.ptt.gov.tr/PttVeriYuklemeTest/services/Sorgu?wsdl',
        ),
        "live" => array(
            'PttVeriYukle2' => 'https://pttws.ptt.gov.tr/PttVeriYukleme/services/Sorgu?wsdl',
            'GonderiHareketV2' => 'https://pttws.ptt.gov.tr/GonderiHareketV2/services/Sorgu?wsdl',
            'GonderiTakipV2' => 'https://pttws.ptt.gov.tr/GonderiTakipV2/services/Sorgu?wsdl',
            'PttBarkodVeriSil' => 'https://pttws.ptt.gov.tr/PttVeriYukleme/services/Sorgu?wsdl',
        ),
    );

    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
        $this->apiAuth = array(
            "kullanici" => 'PttWs',
            "sifre" => $password,
            "musteriId" => $username,
            "musteri_no" => $username,
        );
    }

    public function initClient($method)
    {
        $baseUrl = $this->mod == 'test' ? $this->backendUrl['test'][$method] : $this->backendUrl['live'][$method];
        $this->client = new SoapClient($baseUrl, array('trace' => 1));
    }
    public function kullaniciBilgileri()
    {
        $kullaniciSopa = '';
    }
    /*Fiziken sevk edilmiş, Taşıma irsaliyesi / gönderilerin durumu ve kargo hareketlerinin sorgulanmasına imkan verir. */
    public function listInvDocumentInterfaceWithReference()
    {
        $query = '';
    }

    /* Siparis no ile yeni gönderi oluşturur*/
    public function gonderiOlustur($gonderimBilgileri)
    {

        $extra = array(
            'gonderiTur' => 'KARGO',
            'gonderiTip' => 'NORMAL',
            'dosyaAdi' => $gonderimBilgileri['dosyaAdi']
        );
        $data = array_merge(
            $this->apiAuth,
            $extra,
            array("dongu" => $gonderimBilgileri)
        );
        $this->initClient('PttVeriYukle2');

        return $this->client->kabulEkle2(['input' => $data]);
    }
    /* Siparis no ile yeni gönderiyi iptaleder*/
    public function gonderiyiIptalEt($cargoKeys, $dosyaAdi)
    {
        $data = array_merge(
            $this->apiAuth,
            array("barcode" => $cargoKeys, 'dosyaAdi' => $dosyaAdi)
        );

        $this->initClient('PttBarkodVeriSil');
        return $this->client->barkodVeriSil(['inpDelete' => $data]);
    }
    public function gonderiBilgileriniAl($cargoKey, $tur = 0, $gecmisVerisi = true, $tracking = true)
    {
        $data = array_merge(
            array(
                'kullanici' => $this->apiAuth['musteriId'],
                'sifre' => $this->apiAuth['sifre'],
            ),
            array("referansNo" => $cargoKey)
        );
        $this->initClient('GonderiTakipV2');
        return $this->client->gonderiSorgu_referansNo(['input' => $data]);
    }

    public function detayliGonderiBilgileriniAl($barkodNo, $tur = 0, $gecmisVerisi = true, $tracking = true, $jsonData = true)
    {
        $data = array_merge(
            $this->apiAuth,
            array(
                "keys" => [$barkodNo],
                "keyType" => $tur, // 0 – Kargo 1 – Fatura
                "addHistoricalData" => $gecmisVerisi, // true / false Default : false
                "onlyTracking" => $tracking,
                "jsonData" => $jsonData,
            )
        );

        $this->initClient('shippingOrderDispatcherServices');
        return $this->client->queryShipmentDetail($data);
    }
}
