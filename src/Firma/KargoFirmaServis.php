<?php
namespace KargoApi\Firma;

interface KargoFirmaServis
{
    public function gonderiOlustur($gonderiBilgileri);
    public function gonderiyiIptalEt($gonderiNo, $dosyaAdi);
    public function gonderiBilgileriniAl($barkodNo, $tur, $gecmisVerisi, $tracking);
    public function detayliGonderiBilgileriniAl($barkodNo, $tur, $gecmisVerisi, $tracking, $jsonData);
}
